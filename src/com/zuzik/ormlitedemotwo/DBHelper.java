package com.zuzik.ormlitedemotwo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DBHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "dbcontacts.db";
	private static final int DATABASE_VERSION = 1;
	
	private ContactDAO contactDAO=null;
	
	public DBHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(arg1, Contact.class);
			
			addNewEntriesInDB();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		try {
			TableUtils.dropTable(arg1, Contact.class, true);
			onCreate(arg0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private List<Contact> getRandomContacts(){
		Random r = new Random();
		List<Contact> cnts = new ArrayList<Contact>();
		for (int i = 0; i < 30; i++) {
			int rand = r.nextInt();
			cnts.add(new Contact(Integer.toString(rand),rand));
		}
		return cnts;
	}
	
	public ContactDAO getContactDAO() throws SQLException{
		if(contactDAO==null){
			contactDAO = new ContactDAO(getConnectionSource(), Contact.class);
		}
		return contactDAO;
	}
	
	@Override
	public void close() {
		super.close();
		contactDAO = null;
	}
	
	public void addNewEntriesInDB() throws SQLException{
		getContactDAO().createAll(getRandomContacts());
	}
	
}
