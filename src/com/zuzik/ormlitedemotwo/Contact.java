package com.zuzik.ormlitedemotwo;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=Contact.TABLE_NAME)
public class Contact {

	public static final String TABLE_NAME = "contacts";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_AGE = "age";
	
	@DatabaseField(columnName=COLUMN_ID, generatedId=true, dataType = DataType.INTEGER)
	private int mId;
	
	@DatabaseField(columnName=COLUMN_NAME, dataType = DataType.STRING)
	private String mName;
	
	@DatabaseField(columnName=COLUMN_AGE, dataType = DataType.INTEGER)
	private int mAge;
	
	public Contact(){
	}

	public Contact(String name, int age) {
		super();
		mName = name;
		mAge = age;
	}

	public int getId() {
		return mId;
	}

	public void setId(int id) {
		mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public int getAge() {
		return mAge;
	}

	public void setAge(int age) {
		mAge = age;
	}

	@Override
	public String toString() {
		return "Contact [Id=" + mId + ", Name=" + mName + ", Age=" + mAge
				+ "]";
	}
	
	
}
