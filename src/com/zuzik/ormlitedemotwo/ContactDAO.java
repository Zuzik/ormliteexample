package com.zuzik.ormlitedemotwo;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class ContactDAO extends BaseDaoImpl<Contact, Integer> {

	protected ContactDAO(ConnectionSource connectionSource,
			Class<Contact> dataClass) throws SQLException {
		super(connectionSource, dataClass);
	}

	public void createAll(List<Contact> contacts) throws SQLException{
		for(Contact c: contacts){
			this.create(c);
		}
	}
	
	public List<Contact> getAllDb(String columnName, boolean ascending) throws SQLException{
		return query(this.queryBuilder().orderBy(columnName, ascending).prepare());
	}
	
	public List<Contact> getByLessId(int id) throws SQLException{
		return this.query(getByLessQuery(Contact.COLUMN_ID, id).prepare());
	}
	
	public List<Contact> getByBiggerId(int id) throws SQLException{
		return this.query(getByBiggerQuery(Contact.COLUMN_ID, id).prepare());
	}
	
	public List<Contact> getByName(String name) throws SQLException{
		return this.query(getFindQuery(Contact.COLUMN_NAME, name).prepare());
	}
	
	public List<Contact> getByBiggerName(String name) throws SQLException{
		return this.query(getByBiggerQuery(Contact.COLUMN_NAME, name).prepare());
	}
	
	public List<Contact> getByLessName(String name) throws SQLException{
		return this.query(getByLessQuery(Contact.COLUMN_NAME, name).prepare());
	}
	
	public void removeByName(String name) throws SQLException{
		removeEquals(Contact.COLUMN_NAME, name);
	}
	
	public List<Contact> getByAge(int age) throws SQLException{
		return this.query(getFindQuery(Contact.COLUMN_AGE, age).prepare());
	}
	
	public List<Contact> getByBiggerAge(int age) throws SQLException{
		return this.query(getByBiggerQuery(Contact.COLUMN_AGE, age).prepare());
	}
	
	public List<Contact> getByLessAge(int age) throws SQLException{
		return this.query(getByLessQuery(Contact.COLUMN_AGE, age).prepare());
	}
	
	public void removeByAge(int age) throws SQLException{
		removeEquals(Contact.COLUMN_AGE, age);
	}
	
	
	private QueryBuilder<Contact, Integer> getFindQuery(String column_name, Object value) throws SQLException{
		QueryBuilder<Contact, Integer> q = this.queryBuilder();
		q.where().eq(column_name, value);
		return q;
	}
	
	private QueryBuilder<Contact, Integer> getByLessQuery(String column_name, Object value) throws SQLException{
		QueryBuilder<Contact, Integer> q = this.queryBuilder();
		q.where().lt(column_name, value);
		return q;
	}
	
	private QueryBuilder<Contact, Integer> getByBiggerQuery(String column_name, Object value) throws SQLException{
		QueryBuilder<Contact, Integer> q = this.queryBuilder();
		q.where().gt(column_name, value);
		return q;
	}
	
	private void removeEquals(String column_name, Object value) throws SQLException{
		DeleteBuilder<Contact, Integer> del = this.deleteBuilder();
		del.where().eq(column_name, value);
		del.delete();
	}
	
}
