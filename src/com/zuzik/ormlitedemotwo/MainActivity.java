package com.zuzik.ormlitedemotwo;

import java.sql.SQLException;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	Button buttonShowDB, buttonFind, buttonRemove, buttonLess, buttonBigger, buttonAdd;
	EditText editValue;
	RadioButton radioId, radioName, radioAge;
	TextView textDB;
	Switch switch_asc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		HelperFactory.setHelper(getApplicationContext());
		
		createViews();
		setButtonListeners();
	}
	
	private void createViews(){
		buttonShowDB = (Button) findViewById(R.id.button_show_db);
		buttonFind = (Button) findViewById(R.id.button_find);
		buttonRemove = (Button) findViewById(R.id.button_remove);
		buttonLess = (Button) findViewById(R.id.button_less);
		buttonBigger = (Button) findViewById(R.id.button_bigger);
		buttonAdd = (Button) findViewById(R.id.button_add);
		
		editValue = (EditText) findViewById(R.id.editText_value);
		
		radioId = (RadioButton) findViewById(R.id.radio_id);
		radioName = (RadioButton) findViewById(R.id.radio_name);
		radioAge = (RadioButton) findViewById(R.id.radio_age);
		
		textDB= (TextView) findViewById(R.id.textView_db);
		
		switch_asc = (Switch) findViewById(R.id.switcher);
	}
	
	@Override
	protected void onDestroy() {
		HelperFactory.releaseHelper();
		super.onDestroy();
	}

	private void setButtonListeners(){
		buttonShowDB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				showAllDB();
			}
		});
		buttonAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				addNewEntriesInDB();
			}
		});
		buttonFind.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionButtonFind();
			}
		});
		buttonRemove.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionButtonRemove();
			}
		});
		buttonLess.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionButtonLess();
			}
		});
		buttonBigger.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				actionButtonBigger();
			}
		});
	}
	
	private void showListContacts(List<Contact> contacts){
		StringBuilder sb = new StringBuilder();
		for(Contact c:contacts){
			sb.append(c.toString());
			sb.append("\n");
		}
		textDB.setText(sb.toString());
	}
	
	private void showContact(Contact contact){
		textDB.setText(contact.toString());
	}
	
	private void showToast(String text){
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}
	
	private boolean isIntegerEntered(){
		try{
			int test = getIntegerEntered();
			return true;
		} catch( Exception e){
			return false;
		}
	}
	
	private int getIntegerEntered(){
		return Integer.parseInt(editValue.getText().toString());
	}
	
	private boolean isStringEntered(){
		return editValue.getText().length()>0;
	}
	
	private String getStringEntered(){
		return editValue.getText().toString();
	}
	
	private boolean isAscending(){
		return switch_asc.isChecked();
	}
	
	private String getColumnNameByRadio(){
		if(radioAge.isChecked()){
			return Contact.COLUMN_AGE;
		} else if(radioId.isChecked()){
			return Contact.COLUMN_ID;
		} else if(radioName.isChecked()){
			return Contact.COLUMN_NAME;
		} else {
			return null;
		}
	}
	
	private void showAllDB(){
		try {
			showListContacts(HelperFactory.getHelper().getContactDAO().getAllDb(getColumnNameByRadio(), isAscending()));
		} catch (SQLException e) {
			showToast("������� ��������� ������ �������");
		}
	}

	private void addNewEntriesInDB(){
		try {
			HelperFactory.getHelper().addNewEntriesInDB();
			showAllDB();
			showToast("������ ������");
		} catch (SQLException e) {
			showToast("������� ���������� ��");
		}
	}

	private void actionButtonFind(){
		if(radioId.isChecked()){
			showById();
		}else if(radioName.isChecked()){
			showByName();
		}else if(radioAge.isChecked()){
			showByAge();
		}
	}
	
	private void actionButtonRemove(){
		if(radioId.isChecked()){
			removeById();
		}else if(radioName.isChecked()){
			removeByName();
		}else if(radioAge.isChecked()){
			removeByAge();
		}
	}
	
	private void actionButtonLess(){
		if(radioId.isChecked()){
			showByLessId();
		}else if(radioName.isChecked()){
			showByLessName();
		}else if(radioAge.isChecked()){
			showByLessAge();
		}
	}
	
	private void actionButtonBigger(){
		if(radioId.isChecked()){
			showByBiggerId();
		}else if(radioName.isChecked()){
			showByBiggerName();
		}else if(radioAge.isChecked()){
			showByBiggerAge();
		}
	}
	
	private void showById(){
		if(isIntegerEntered()){
			int id = getIntegerEntered();
			try {
				Contact c = HelperFactory.getHelper().getContactDAO().queryForId(id);
				if(c!=null){
					showContact(c);
				} else {
					showToast("�� �������� ������ �� ��������� ID");
				}
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ID");
			}
		}else {
			showToast("�� ������� ID");
		}
	}
	
	private void removeById(){
		if(isIntegerEntered()){
			int id = getIntegerEntered();
			try {
				HelperFactory.getHelper().getContactDAO().deleteById(id);
				showToast("����� ��������");
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ID");
			}
		}else {
			showToast("�� ������� ID");
		}
	}

	private void showByLessId(){
		if(isIntegerEntered()){
			int id = getIntegerEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByLessId(id));
			} catch (SQLException e) {
				showToast("������� ��������� ������ � ������ ID");
			}
		}else {
			showToast("�� ������� ID");
		}
	}
	
	private void showByBiggerId(){
		if(isIntegerEntered()){
			int id = getIntegerEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByBiggerId(id));
			} catch (SQLException e) {
				showToast("������� ��������� ������ � ������ ID");
			}
		}else {
			showToast("�� ������� ID");
		}
	}

	private void showByName(){
		if(isStringEntered()){
			String name = getStringEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByName(name));
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� �����");
			}
		}else {
			showToast("�� ������� �����");
		}
	}
	
	private void showByLessName(){
		if(isStringEntered()){
			String name = getStringEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByLessName(name));
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ������� �����");
			}
		}else {
			showToast("�� ������� �����");
		}
	}
	
	private void showByBiggerName(){
		if(isStringEntered()){
			String name = getStringEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByBiggerName(name));
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ������� �����");
			}
		}else {
			showToast("�� ������� �����");
		}
	}
	
	private void removeByName(){
		if(isStringEntered()){
			String name = getStringEntered();
			try {
				HelperFactory.getHelper().getContactDAO().removeByName(name);
				showToast("����� ��������");
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� �����");
			}
		}else {
			showToast("�� ������� �����");
		}
	}

	private void showByAge(){
		if(isIntegerEntered()){
			int age = getIntegerEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByAge(age));
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ���");
			}
		}else {
			showToast("�� ������� ��");
		}
	}
	
	private void removeByAge(){
		if(isIntegerEntered()){
			int age = getIntegerEntered();
			try {
				HelperFactory.getHelper().getContactDAO().removeByAge(age);
				showToast("����� ��������");
			} catch (SQLException e) {
				showToast("������� ��������� ������ �� ���");
			}
		}else {
			showToast("�� ������� ��");
		}
	}

	private void showByLessAge(){
		if(isIntegerEntered()){
			int age = getIntegerEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByLessAge(age));
			} catch (SQLException e) {
				showToast("������� ��������� ������ � ������ ����");
			}
		}else {
			showToast("�� ������� ��");
		}
	}
	
	private void showByBiggerAge(){
		if(isIntegerEntered()){
			int age = getIntegerEntered();
			try {
				showListContacts(HelperFactory.getHelper().getContactDAO().getByBiggerAge(age));
			} catch (SQLException e) {
				showToast("������� ��������� ������ � ������ ����");
			}
		}else {
			showToast("�� ������� ����");
		}
	}

}
